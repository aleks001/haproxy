# General

This project summerize the differnt haproxy repos from me.

## haproxy 1.8 on centos
https://gitlab.com/aleks001/haproxy18-centos

### Docker image
https://hub.docker.com/r/me2digital/haproxy18/

## haproxy 1.6 on centos
https://gitlab.com/aleks001/haproxy16-centos

## haproxy 1.6 on rhel7
https://gitlab.com/aleks001/haproxy16-rhel7

## haproxy 1.7 on centos
https://gitlab.com/aleks001/haproxy17-centos

### Docker image
https://hub.docker.com/r/me2digital/haproxy17/

## haproxy 1.7 on rhel7
https://gitlab.com/aleks001/haproxy17-rhel7

## Openshift origin router with haproxy 1.7
https://gitlab.com/aleks001/openshift-origin-router-hap17

### Docker image
https://hub.docker.com/r/me2digital/openshift-origin-router-hap17/

## Openshift enterprise router with haproxy 1.7
https://gitlab.com/aleks001/openshift-oscp-router-hap17

### Attention
You will need a valid Openshift enterprise subscription to build the 
Openshift enterprise router.